/*
 *  swiftpro_node.cpp
 *  swiftpro
 *  
 *  Created by Patryk Cieslak on 19/01/2023.
 *  Copyright (c) 2023 Patryk Cieslak. All rights reserved.
 */

#include <ros/ros.h>
#include <ros/console.h>
#include <std_msgs/Bool.h>
#include <std_srvs/Empty.h>
#include <std_srvs/SetBool.h>
#include <hardware_interface/robot_hw.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <controller_manager/controller_manager.h>
#include <swiftpro/swiftpro.h>

class SwiftProNode : public hardware_interface::RobotHW
{
public:
	SwiftProNode(const ros::NodeHandle& nh) : nh_(nh), swift_(nullptr)
	{
		if(!getConfig())
		{
			throw std::runtime_error("Missing parameters!");
		}
		
		// Create publishers and services
		ee_pump_pub_ = nh_.advertise<std_msgs::Bool>("vacuum_gripper/pump_state", 10);
		ee_switch_pub_ = nh_.advertise<std_msgs::Bool>("vacuum_gripper/limit_switch_state", 10);
		pump_srv_ = nh_.advertiseService("vacuum_gripper/set_pump", &SwiftProNode::setPump, this);
		pump_state_.data = false;	
		limit_switch_state_.data = false;
		arm_drives_srv_ = nh_.advertiseService("arm", &SwiftProNode::arm, this);
		disarm_drives_srv_= nh_.advertiseService("disarm", &SwiftProNode::disarm, this);

		// Setup ROS control
		cmd_.resize(4, 0.0);
		pos_.resize(4, 0.0);
		vel_.resize(4, 0.0);
		eff_.resize(4, 0.0);

		ROS_INFO_STREAM("Namespace: " << config_joint_ns_);

		for(size_t i=0; i<4; ++i)
		{
			hardware_interface::JointStateHandle state_handle(config_joint_ns_ + "/joint" + std::to_string(i+1), &pos_[i], &vel_[i], &eff_[i]);
			jsif_.registerHandle(state_handle);
		}
		registerInterface(&jsif_);

		for(size_t i=0; i<4; ++i)
		{
			hardware_interface::JointHandle cmd_handle(jsif_.getHandle(config_joint_ns_ + "/joint" + std::to_string(i+1)), &cmd_[i]);
			jvif_.registerHandle(cmd_handle);
		}
		registerInterface(&jvif_);
	}

	virtual ~SwiftProNode()
	{
		if(swift_ != nullptr)
			delete swift_;
	}

	bool getConfig()
	{
		bool success = nh_.getParam("serial_port", config_serial_port_);
		success = success && nh_.getParam("namespace", config_joint_ns_);
		return success;
	}

	void connect(const std::function<void()>& update_callback)
	{
		// Connect to hardware
		swift_ = new SwiftPro(update_callback);

		if (!swift_->connect(config_serial_port_))
		{
			throw std::runtime_error("Unable to open port '" + config_serial_port_ + "'");
		}
		else
		{
			ROS_INFO_STREAM("Manipulator connected on port '" + config_serial_port_ + "'");
		}

		if(!swift_->arm())
		{
			throw std::runtime_error("Unable to arm drives!");
		}
		else
		{
			ROS_INFO_STREAM("Drives armed.");
		}

		swift_->setAutoupdate(true);
	}

	void read()
	{
		// Robot in simulation was built with Z axis down !!!
		std::vector<double> pos = swift_->getJointPositions();
		pos_[0] = -pos[0];
		pos_[1] = -pos[1];
		pos_[2] = -pos[2];
		pos_[3] = -pos[3];
		pump_state_.data = swift_->getPumpState();
		limit_switch_state_.data = swift_->getLimitSwitchState();
		ee_pump_pub_.publish(pump_state_);
		ee_switch_pub_.publish(limit_switch_state_);
	}

	void write()
	{
		if(cmd_[0] == 0.0 && cmd_[1] == 0.0
			&& cmd_[2] == 0.0 && cmd_[3] == 0.0)
		{
			swift_->stop();
		}
		else
		{
			// Robot in simulation was built with Z axis down !!!
			std::vector<double> cmd(4);
			cmd[0] = -cmd_[0];
			cmd[1] = -cmd_[1];
			cmd[2] = -cmd_[2];
			cmd[3] = -cmd_[3];
			swift_->requestJointVelocities(cmd);
		}
	}
	
	bool arm(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res)
	{
		swift_->setAutoupdate(false);
		if(!swift_->arm())
			ROS_ERROR("Arming drives failed!");
		else 
			ROS_INFO("Drives armed.");
		swift_->setAutoupdate(true);
		return true;
	}

	bool disarm(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res)
	{
		swift_->setAutoupdate(false);
		if(!swift_->disarm())
			ROS_ERROR("Disarming drives failed!");
		else
			ROS_INFO("Drives disarmed.");
		swift_->setAutoupdate(true);
		return true;
	}

	bool setPump(std_srvs::SetBool::Request& req, std_srvs::SetBool::Response& res)
	{
		if(req.data)
		{
			swift_->pumpOn();
			res.message = "Pump on.";
		}
		else
		{
			swift_->pumpOff();
			res.message = "Pump off.";
		}
		res.success = true;
		return true;
	}

private:
	SwiftPro* swift_;
	std::string config_serial_port_;
	std::string config_joint_ns_;
	
	controller_manager::ControllerManager* cm_;
	hardware_interface::JointStateInterface jsif_;
	hardware_interface::VelocityJointInterface jvif_;
	std::vector<double> cmd_;
	std::vector<double> eff_;
	std::vector<double> vel_;
	std::vector<double> pos_;
	
	ros::NodeHandle nh_;
	ros::Publisher ee_pump_pub_;
	ros::Publisher ee_switch_pub_;
	std_msgs::Bool pump_state_;
	std_msgs::Bool limit_switch_state_;
	ros::ServiceServer pump_srv_;
	ros::ServiceServer arm_drives_srv_;
	ros::ServiceServer disarm_drives_srv_;
};

SwiftProNode* g_swift = nullptr;
controller_manager::ControllerManager* g_cm = nullptr;
ros::Time g_last_time;

void update()
{
	g_swift->read();
	ros::Time now = ros::Time::now();
	g_cm->update(now, now-g_last_time);
	g_last_time = now;
	g_swift->write();
}

int main(int argc, char** argv)
{	
	ros::init(argc, argv, "swiftpro_node");
	ros::NodeHandle nh("~");

	ros::AsyncSpinner spinner(4);
	spinner.start();
    	
	try
	{
		g_swift = new SwiftProNode(nh);
		g_cm = new controller_manager::ControllerManager(g_swift, nh);
		g_last_time = ros::Time::now();
		g_swift->connect(update);
		ros::waitForShutdown();
	}
	catch(std::runtime_error& e)
	{
		std::cout << e.what() << std::endl;
	}

	spinner.stop();

	if(g_swift)
		delete g_swift;
	if(g_cm)
		delete g_cm;

	return 0;
}